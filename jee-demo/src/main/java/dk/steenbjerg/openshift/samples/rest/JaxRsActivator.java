package dk.steenbjerg.openshift.samples.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/rest")
public class JaxRsActivator extends Application {
	// intentional left empty
}
 
