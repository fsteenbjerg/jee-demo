package dk.steenbjerg.openshift.samples.rest;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/infos")
@RequestScoped
public class InfoResource {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String showInfo() {
        return "Hallo this is right";
    }
}
